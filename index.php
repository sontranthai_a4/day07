<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search</title>
    <!-- <link rel="stylesheet" href="css/styles.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <style>
        .cborder {
            border: 2px solid blue;
            border-radius: 0;
            box-sizing: border-box;
            background-color: #bbd4fc;
        }

        .cbutton {
            border: 2px solid blue;
            border-radius: 15px;
            box-sizing: border-box;
            background-color: #3385ff;
            font-size: 20px;
            color: white;
        }

        .borderless td,
        .borderless th {
            border: none;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="mx-auto py-5" style="width: 50%">
            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label" for="">Khoa</label>
                <div class="col-sm-10">
                    <select class="form-control cborder" name="" id="">
                        <?php
                        $DSKHOA = array(
                            null => "",
                            "MAT" => "Khoa học máy tính",
                            "KDL" => "Khoa học vật liệu"
                        );
                        foreach ($DSKHOA as $key => $khoa) {
                            echo "<option>$khoa</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label" for="">Từ khóa</label>
                <div class="col-sm-10">
                    <input class="form-control cborder" type="text" placeholder="">
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary cbutton">Tìm kiếm</button>
            </div>
            <div class="py-3">Số sinh viên tìm thấy: XXX</div>
            <div class="d-flex flex-row-reverse">
                <button id="newItem" type="submit" class="btn btn-primary cbutton" style="font-size: 16px;">Thêm</button>
                <script>
                    document.getElementById("newItem").onclick = function() {
                        location.href = "register.php";
                    };
                </script>
            </div>
        </div>
        <div class="mx-auto" style="width:75%">
            <table class="table table-responsive borderless">
                <tr>
                    <td>No</td>
                    <td>Tên sinh viên</td>
                    <td>Khoa</td>
                    <td>Action</td>
                </tr>
                <?php
                $RESULT = array(
                    array(1, "Nguyễn Văn A", "Khoa học máy tính"),
                    array(2, "Trần Thị B", "Khoa học máy tính"),
                    array(3, "Nguyễn Hoàng C", "Khoa học vật liệu"),
                    array(4, "Đinh Quang D", "Khoa học vật liệu"),
                );
                foreach ($RESULT as $student) {
                    echo "
                <tr>
                    <td>$student[0]</td>
                    <td>$student[1]</td>
                    <td>$student[2]</td>
                    <td>
                        <button class=\"cborder\" style=\"color: white;\">Xóa</button>
                        <button class=\"cborder\" style=\"color: white;\">Sửa</button>
                    </td>
                </tr>
                ";
                }
                ?>
            </table>
        </div>
    </div>
</body>

</html>